package com.training3.example_services;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity implements View.OnClickListener {


    EditText mFirst,mSecond;

    Button mAdd,mSubtract,mClear;

    TextView mResultText;

    protected IRemote mService;

    ServiceConnection mServiceConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        mFirst = (EditText) findViewById(R.id.firstValue);

        mSecond = (EditText) findViewById(R.id.secondValue);

        mResultText = (TextView) findViewById(R.id.resultText);

        mAdd = (Button) findViewById(R.id.add);

        mAdd.setOnClickListener(this);



        initConnection();
    }

    void initConnection(){

        mServiceConnection = new ServiceConnection() {



            @Override

            public void onServiceDisconnected(ComponentName name) {

                // TODO Auto-generated method stub

                mService = null;

                Toast.makeText(getApplicationContext(), "no", Toast.LENGTH_SHORT).show();

                Log.d("IRemote", "Binding - Service disconnected");

            }



            @Override

            public void onServiceConnected(ComponentName name, IBinder service)

            {

                // TODO Auto-generated method stub

                mService = IRemote.Stub.asInterface((IBinder) service);

                Toast.makeText(getApplicationContext(), "yes", Toast.LENGTH_SHORT).show();

                Log.d("IRemote", "Binding is done - Service connected");

            }

        };

        if(mService == null)

        {


            Intent it = new Intent(getApplicationContext(),ArithmeticService.class);
            bindService(it, mServiceConnection, Service.BIND_AUTO_CREATE);

        }

    }

    protected void onDestroy() {

        super.onDestroy();

        unbindService(mServiceConnection);

    };



    @Override

    public void onClick(View v) {

        // TODO Auto-generated method stub

        switch(v.getId()){

            case R.id.add:{

                int a = Integer.parseInt(mFirst.getText().toString());

                int b = Integer.parseInt(mSecond.getText().toString());



                try{

                    mResultText.setText("Result -> Add ->"+mService.add(a,b));

                    Log.d("IRemote", "Binding - Add operation");

                } catch (RemoteException e) {

                    // TODO Auto-generated catch block

                    e.printStackTrace();

                }

            }

            break;

        }

    }
}
